package com.atlassian.plugin.connect.confluence.capabilities.provider;

import com.atlassian.plugin.connect.modules.beans.ContentPropertyModuleBean;
import com.atlassian.plugin.connect.spi.module.provider.ConnectModuleProvider;

public interface ContentPropertyModuleProvider
        extends ConnectModuleProvider<ContentPropertyModuleBean>
{
}
