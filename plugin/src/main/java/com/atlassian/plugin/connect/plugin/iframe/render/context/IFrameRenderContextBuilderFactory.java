package com.atlassian.plugin.connect.plugin.iframe.render.context;

/**
 *
 */
public interface IFrameRenderContextBuilderFactory
{
    IFrameRenderContextBuilder builder();
}
