package com.atlassian.plugin.connect.jira.capabilities.provider;

import com.atlassian.plugin.connect.modules.beans.WorkflowPostFunctionModuleBean;
import com.atlassian.plugin.connect.spi.module.provider.ConnectModuleProvider;

public interface WorkflowPostFunctionModuleProvider extends ConnectModuleProvider<WorkflowPostFunctionModuleBean>
{
}
