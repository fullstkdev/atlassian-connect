package com.atlassian.plugin.connect.plugin.capabilities.descriptor;

import com.atlassian.plugin.connect.spi.capabilities.descriptor.ConnectModuleDescriptorFactory;
import com.atlassian.plugin.connect.modules.beans.WebSectionModuleBean;
import com.atlassian.plugin.web.descriptors.WebSectionModuleDescriptor;

public interface ConnectWebSectionModuleDescriptorFactory extends ConnectModuleDescriptorFactory<WebSectionModuleBean, WebSectionModuleDescriptor>
{

}
