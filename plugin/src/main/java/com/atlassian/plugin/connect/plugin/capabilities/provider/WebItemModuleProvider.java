package com.atlassian.plugin.connect.plugin.capabilities.provider;

import com.atlassian.plugin.connect.modules.beans.WebItemModuleBean;
import com.atlassian.plugin.connect.spi.module.provider.ConnectModuleProvider;

public interface WebItemModuleProvider extends ConnectModuleProvider<WebItemModuleBean>
{

}
