package com.atlassian.plugin.connect.jira.bridge;

import com.atlassian.jira.compatibility.factory.plugin.issuetabpanel.ShowPanelRequestHelperBridgeFactory;
import com.atlassian.plugin.spring.scanner.annotation.component.JiraComponent;

@JiraComponent
public class ConnectShowPanelRequestHelperBridgeFactory extends ShowPanelRequestHelperBridgeFactory
{
}
