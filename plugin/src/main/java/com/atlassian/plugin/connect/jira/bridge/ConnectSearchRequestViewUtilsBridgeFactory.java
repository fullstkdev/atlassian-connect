package com.atlassian.plugin.connect.jira.bridge;

import com.atlassian.jira.compatibility.factory.issue.views.SearchRequestViewUtilsBridgeFactory;
import com.atlassian.plugin.spring.scanner.annotation.component.JiraComponent;

@JiraComponent
public class ConnectSearchRequestViewUtilsBridgeFactory extends SearchRequestViewUtilsBridgeFactory
{
}
