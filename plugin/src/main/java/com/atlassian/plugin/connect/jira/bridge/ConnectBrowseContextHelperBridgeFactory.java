package com.atlassian.plugin.connect.jira.bridge;

import com.atlassian.jira.compatibility.factory.project.browse.BrowseContextHelperBridgeFactory;
import com.atlassian.plugin.spring.scanner.annotation.component.JiraComponent;

@JiraComponent
public class ConnectBrowseContextHelperBridgeFactory extends BrowseContextHelperBridgeFactory
{ }
