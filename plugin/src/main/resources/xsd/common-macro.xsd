<?xml version="1.0" encoding="UTF-8" ?>
<xs:schema xmlns:xs="http://www.w3.org/2001/XMLSchema">
    <xs:include schemaLocation="common.xsd"/>
    <xs:include schemaLocation="atlassian-plugin-common.xsd"/>

    <xs:simpleType name="MacroOutputTypeType">
        <xs:restriction base="xs:string">
            <xs:enumeration value="block">
                <xs:annotation>
                    <xs:documentation>
                        If the macro output should be displayed on a new line as a block
                    </xs:documentation>
                </xs:annotation>
            </xs:enumeration>
            <xs:enumeration value="inline">
                <xs:annotation>
                    <xs:documentation>
                        If the macro output should be displayed within the existing content
                    </xs:documentation>
                </xs:annotation>
            </xs:enumeration>
        </xs:restriction>
    </xs:simpleType>

    <xs:simpleType name="MacroContextParameterNameType">
        <xs:restriction base="xs:string">
            <xs:enumeration value="output_type">
                <xs:annotation>
                    <xs:documentation>
                        <![CDATA[
                        Possible values include:<ul>
                        <li>'preview'</li>
                        <li>'display'</li>
                        <li>'word' (Microsoft Word export)</li>
                        <li>'pdf' (PDF export)</li>
                        <li>'html_export'</li>
                        </ul>
                        <]]>
                    </xs:documentation>
                </xs:annotation>
            </xs:enumeration>
            <xs:enumeration value="page_id" />
            <xs:enumeration value="page_type">
                <xs:annotation>
                    <xs:documentation>
                        The type of the content, such as 'page', 'blogpost', or 'comment'
                    </xs:documentation>
                </xs:annotation>
            </xs:enumeration>
            <xs:enumeration value="page_title" />
            <xs:enumeration value="user_id" />
            <xs:enumeration value="user_key" />
        </xs:restriction>
    </xs:simpleType>

    <xs:simpleType name="MacroBodyTypeType">
        <xs:restriction base="xs:string">
            <xs:enumeration value="rich-text">
                <xs:annotation>
                    <xs:documentation>
                        If this macro allows its body to contain rich content such as wiki markup
                    </xs:documentation>
                </xs:annotation>
            </xs:enumeration>
            <xs:enumeration value="plain-text">
                <xs:annotation>
                    <xs:documentation>
                        If this macro can only contain plain text
                    </xs:documentation>
                </xs:annotation>
            </xs:enumeration>
            <xs:enumeration value="none">
                <xs:annotation>
                    <xs:documentation>
                        If this macro has no body
                    </xs:documentation>
                </xs:annotation>
            </xs:enumeration>
        </xs:restriction>
    </xs:simpleType>

    <xs:simpleType name="CategoryNameType">
        <xs:restriction base="xs:string">
            <xs:enumeration value="formatting"/>
            <xs:enumeration value="confluence-content"/>
            <xs:enumeration value="media"/>
            <xs:enumeration value="visuals"/>
            <xs:enumeration value="navigation"/>
            <xs:enumeration value="external-content"/>
            <xs:enumeration value="communication"/>
            <xs:enumeration value="reporting"/>
            <xs:enumeration value="admin"/>
            <xs:enumeration value="development"/>
        </xs:restriction>
    </xs:simpleType>

    <xs:complexType name="CategoryType">
        <xs:attribute name="name" type="CategoryNameType" use="required"/>
    </xs:complexType>

    <xs:complexType name="MacroParameterAliasType">
        <xs:attribute name="name" type="xs:string" use="required"/>
    </xs:complexType>

    <xs:complexType name="MacroParameterEnumValueType">
        <xs:attribute name="name" type="xs:string" use="required"/>
    </xs:complexType>

    <xs:complexType name="MacroParameterType">
        <xs:sequence>
            <xs:element name="description" minOccurs="0" maxOccurs="1" type="DescriptionType">
                <xs:annotation>
                    <xs:documentation>
                        The description that will show up in the macro editor for this parameter
                    </xs:documentation>
                </xs:annotation>
            </xs:element>
            <xs:element name="alias" type="MacroParameterAliasType" minOccurs="0" maxOccurs="1">
                <xs:annotation>
                    <xs:documentation>
                        The alias for the macro parameter
                    </xs:documentation>
                </xs:annotation>
            </xs:element>
            <xs:element name="value" type="MacroParameterEnumValueType" minOccurs="0" maxOccurs="unbounded">
                <xs:annotation>
                    <xs:documentation>
                        An enum value for this parameter, only valid for an enum parameter type
                    </xs:documentation>
                </xs:annotation>
            </xs:element>
        </xs:sequence>
        <xs:attribute name="name" type="KeyType" use="required">
            <xs:annotation>
                <xs:documentation>
                    The parameter name to be passed to the macro url as a query string.
                </xs:documentation>
            </xs:annotation>
        </xs:attribute>
        <xs:attribute name="title" type="NameType" use="optional">
            <xs:annotation>
                <xs:documentation>
                    The parameter title that is shown in the macro editor
                </xs:documentation>
            </xs:annotation>
        </xs:attribute>
        <xs:attribute name="type" type="MacroParameterTypeType" use="required">
            <xs:annotation>
                <xs:documentation>
                    The type of parameter, used to customize how the parameter is displayed in the editor
                </xs:documentation>
            </xs:annotation>
        </xs:attribute>
        <xs:attribute name="required" type="xs:boolean" default="false">
            <xs:annotation>
                <xs:documentation>
                    Whether this parameter is required or not
                </xs:documentation>
            </xs:annotation>
        </xs:attribute>
        <xs:attribute name="multiple" type="xs:boolean" default="false">
            <xs:annotation>
                <xs:documentation>
                    Whether this parameter can contain multiple values or not.  Only applicable to some parameter types.
                </xs:documentation>
            </xs:annotation>
        </xs:attribute>
        <xs:attribute name="default" type="xs:string">
            <xs:annotation>
                <xs:documentation>
                    The default value of the parameter
                </xs:documentation>
            </xs:annotation>
        </xs:attribute>
    </xs:complexType>

    <xs:simpleType name="MacroParameterTypeType">
        <xs:restriction base="xs:string">
            <xs:enumeration value="enum">
                <xs:annotation>
                    <xs:documentation>
                        Displays a select field
                    </xs:documentation>
                </xs:annotation>
            </xs:enumeration>
            <xs:enumeration value="boolean">
                <xs:annotation>
                    <xs:documentation>
                         Displays a check box
                    </xs:documentation>
                </xs:annotation>
            </xs:enumeration>
            <xs:enumeration value="string">
                <xs:annotation>
                    <xs:documentation>
                         Displays an input field (this is the default if unknown type)
                    </xs:documentation>
                </xs:annotation>
            </xs:enumeration>
            <xs:enumeration value="spacekey">
                <xs:annotation>
                    <xs:documentation>
                         Displays an auto-complete field for search on space names
                    </xs:documentation>
                </xs:annotation>
            </xs:enumeration>
            <xs:enumeration value="attachment">
                <xs:annotation>
                    <xs:documentation>
                         Displays an autocomplete field for search on attachment filenames
                    </xs:documentation>
                </xs:annotation>
            </xs:enumeration>
            <xs:enumeration value="username">
                <xs:annotation>
                    <xs:documentation>
                         Displays an autocomplete field for search on username and full name
                    </xs:documentation>
                </xs:annotation>
            </xs:enumeration>
            <xs:enumeration value="confluence-content">
                <xs:annotation>
                    <xs:documentation>
                        Displays an autocomplete field for search on page and blog titles
                    </xs:documentation>
                </xs:annotation>
            </xs:enumeration>
            <xs:enumeration value="int">
                <xs:annotation>
                    <xs:documentation>
                        Displays an input field
                    </xs:documentation>
                </xs:annotation>
            </xs:enumeration>
        </xs:restriction>
    </xs:simpleType>

    <xs:complexType name="MacroParametersType">
        <xs:sequence minOccurs="1" maxOccurs="unbounded">
            <xs:choice>
                <xs:element name="parameter" type="MacroParameterType"/>
            </xs:choice>
        </xs:sequence>
    </xs:complexType>

    <xs:complexType name="MacroImagePlaceholderType">
        <xs:attribute name="url" type="RelativeUrlType" use="required">
            <xs:annotation>
                <xs:documentation>
                     The URL that returns an image
                </xs:documentation>
            </xs:annotation>
        </xs:attribute>
        <xs:attribute name="width" type="PixelType">
            <xs:annotation>
                <xs:documentation>
                     The width of the image
                </xs:documentation>
            </xs:annotation>
        </xs:attribute>
        <xs:attribute name="height" type="PixelType">
            <xs:annotation>
                <xs:documentation>
                     The height of the image
                </xs:documentation>
            </xs:annotation>
        </xs:attribute>
        <xs:attribute name="apply-chrome" type="xs:boolean" default="true">
            <xs:annotation>
                <xs:documentation>
                     Whether to apply the usual macro placeholder chrome
                </xs:documentation>
            </xs:annotation>
        </xs:attribute>
    </xs:complexType>

    <xs:complexType name="MacroEditorType">
        <xs:attribute name="url" type="RelativeUrlType" use="required">
            <xs:annotation>
                <xs:documentation>
                     The URL to an HTML page that will be placed in an IFrame as a remote dialog
                </xs:documentation>
            </xs:annotation>
        </xs:attribute>
        <xs:attribute name="width" type="PixelType">
            <xs:annotation>
                <xs:documentation>
                     The dialog width
                </xs:documentation>
            </xs:annotation>
        </xs:attribute>
        <xs:attribute name="height" type="PixelType">
            <xs:annotation>
                <xs:documentation>
                     The dialog height
                </xs:documentation>
            </xs:annotation>
        </xs:attribute>
    </xs:complexType>

    <xs:complexType name="AbstractMacroType" abstract="true">
        <xs:complexContent>
            <xs:extension base="AbstractPluginModuleType">
                <xs:sequence>
                    <xs:element name="alias" type="KeyType" minOccurs="0" maxOccurs="unbounded">
                        <xs:annotation>
                            <xs:documentation>
                                 An alias for the macro, used for auto-completion
                            </xs:documentation>
                        </xs:annotation>
                    </xs:element>
                    <xs:element name="category" type="CategoryType" minOccurs="0" maxOccurs="unbounded">
                        <xs:annotation>
                            <xs:documentation>
                                 The category in which to place the macro in the macro editor
                            </xs:documentation>
                        </xs:annotation>
                    </xs:element>
                    <xs:element name="image-placeholder" type="MacroImagePlaceholderType" minOccurs="0" maxOccurs="1">
                        <xs:annotation>
                            <xs:documentation>
                                 The image placeholder to show in the editor
                            </xs:documentation>
                        </xs:annotation>
                    </xs:element>
                    <xs:element name="macro-editor" type="MacroEditorType" minOccurs="0" maxOccurs="1">
                        <xs:annotation>
                            <xs:documentation>
                                 The custom dialog to use when editing the macro
                            </xs:documentation>
                        </xs:annotation>
                    </xs:element>
                    <xs:element name="parameters" type="MacroParametersType" minOccurs="0" maxOccurs="1">
                        <xs:annotation>
                            <xs:documentation>
                                 The parameters to allow the user to use to customize the macro
                            </xs:documentation>
                        </xs:annotation>
                    </xs:element>
                </xs:sequence>
                <xs:attribute name="featured" type="xs:boolean">
                    <xs:annotation>
                        <xs:documentation>
                            Whether the macro should be "featured", meaning having an additional link in the "Insert"
                            menu in the editor toolbar
                        </xs:documentation>
                    </xs:annotation>
                </xs:attribute>
                <xs:attribute name="url" type="RelativeUrlType" use="required">
                    <xs:annotation>
                        <xs:documentation>
                             The URL to use to retrieve the macro content
                        </xs:documentation>
                    </xs:annotation>
                </xs:attribute>
                <xs:attribute name="icon-url" type="RelativeUrlType">
                    <xs:annotation>
                        <xs:documentation>
                            Relative url of an 80x80 pixel image
                        </xs:documentation>
                    </xs:annotation>
                </xs:attribute>
                <xs:attribute name="documentation-url" type="RelativeUrlType">
                    <xs:annotation>
                        <xs:documentation>
                             The URL to show in the macro editor for documentation
                        </xs:documentation>
                    </xs:annotation>
                </xs:attribute>
                <xs:attribute name="output-type" type="MacroOutputTypeType" default="block">
                    <xs:annotation>
                        <xs:documentation>
                             How this macro should be placed along side other page content
                        </xs:documentation>
                    </xs:annotation>
                </xs:attribute>
                <xs:attribute name="body-type" type="MacroBodyTypeType" default="rich-text">
                    <xs:annotation>
                        <xs:documentation>
                             The type of body content, if any, for this macro
                        </xs:documentation>
                    </xs:annotation>
                </xs:attribute>
            </xs:extension>
        </xs:complexContent>
    </xs:complexType>
</xs:schema>