package com.atlassian.plugin.connect.spi;

public class Filenames
{
    public static final String ATLASSIAN_ADD_ON_JSON = "atlassian-connect.json";
    public static final String ATLASSIAN_PLUGIN_XML = "atlassian-plugin.xml";
}
