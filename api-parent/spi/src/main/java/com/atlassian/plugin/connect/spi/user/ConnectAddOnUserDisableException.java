package com.atlassian.plugin.connect.spi.user;

public class ConnectAddOnUserDisableException extends Exception
{
    public ConnectAddOnUserDisableException(Exception cause)
    {
        super(cause);
    }
}
