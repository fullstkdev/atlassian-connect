package com.atlassian.plugin.connect.api.iframe.render.strategy;

public interface IFrameRenderStrategyBuilderFactory
{
    IFrameRenderStrategyBuilder builder();
}
