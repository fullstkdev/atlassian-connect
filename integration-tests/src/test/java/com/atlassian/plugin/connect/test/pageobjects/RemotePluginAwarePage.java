package com.atlassian.plugin.connect.test.pageobjects;

public interface RemotePluginAwarePage
{
    ConnectAddOnEmbeddedTestPage clickAddOnLink();
}
